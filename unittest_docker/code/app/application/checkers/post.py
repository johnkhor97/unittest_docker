def check_length(key, value) -> bool:
    if key == 'title':
        if len(value) == 0 or len(value) > 64:
            return False
        else:
            return True
    elif key == 'content':
        if len(value) < 15 or len(value) > 256:
            return False
        else:
            return True


def post_params_check(content):
    keys = [
        'title',
        'content'
    ]

    if content is None:
        return 'title', False

    for key in keys:
        if not key in content.keys():
            return key, False
        if not check_length(key, content[key]):
            return key, False

    return "ok", True