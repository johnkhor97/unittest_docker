import re


def match_document_number(document_number) -> bool:
    # 检验长度以及形式
    if len(document_number) != 18:
        return False
    if not re.match(r"^\d{17}(\d|X|x)$", document_number):
        return False

    area = {"11": "北京", "12": "天津", "13": "河北", "14": "山西", "15": "内蒙古", "21": "辽宁", "22": "吉林", "23": "黑龙江",
            "31": "上海", "32": "江苏", "33": "浙江", "34": "安徽", "35": "福建", "36": "江西", "37": "山东", "41": "河南", "42": "湖北",
            "43": "湖南", "44": "广东", "45": "广西", "46": "海南", "50": "重庆", "51": "四川", "52": "贵州", "53": "云南", "54": "西藏",
            "61": "陕西", "62": "甘肃", "63": "青海", "64": "宁夏", "65": "新疆", "71": "台湾", "81": "香港", "82": "澳门", "91": "国外"}
    idcard = str(document_number)
    idcard = idcard.strip()
    idcard_list = list(idcard)
    year = int(idcard[6:10])
    if year > 2002:
        return False
    # 地区校验
    if (idcard)[0:2] not in area:
        return False
    # 15位身份号码检测
    if (len(idcard) == 15):
        if ((int(idcard[6:8]) + 1900) % 4 == 0 or (
                (int(idcard[6:8]) + 1900) % 100 == 0 and (int(idcard[6:8]) + 1900) % 4 == 0)):
            erg = re.compile(
                '[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$')  # //测试出生日期的合法性
        else:
            ereg = re.compile(
                '[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$')  # //测试出生日期的合法性
        if (re.match(ereg, idcard)):
            return False
        else:
            return False
    # 18位身份号码检测
    elif (len(idcard) == 18):
        if (int(idcard[6:10]) % 4 == 0 or (int(idcard[6:10]) % 100 == 0 and int(idcard[6:10]) % 4 == 0)):
            ereg = re.compile(
                '[1-9][0-9]{5}(19[0-9]{2}|20[0-9]{2})((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$')  # //闰年出生日期的合法性正则表达式
        else:
            ereg = re.compile(
                '[1-9][0-9]{5}(19[0-9]{2}|20[0-9]{2})((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$')  # //平年出生日期的合法性正则表达式
        # 测试出生日期的合法性
        if (re.match(ereg, idcard)):
            # 计算校验位
            S = (int(idcard_list[0]) + int(idcard_list[10])) * 7 + (int(idcard_list[1]) + int(idcard_list[11])) * 9 + (
            int(idcard_list[2]) + int(idcard_list[12])) * 10 + (
            int(idcard_list[3]) + int(idcard_list[13])) * 5 + (
            int(idcard_list[4]) + int(idcard_list[14])) * 8 + (
            int(idcard_list[5]) + int(idcard_list[15])) * 4 + (
            int(idcard_list[6]) + int(idcard_list[16])) * 2 + int(idcard_list[7]) * 1 + int(
            idcard_list[8]) * 6 + int(idcard_list[9]) * 3
            Y = S % 11
            M = "F"
            JYM = "10X98765432"
            # 判断校验位
            M = JYM[Y]
            # 检测ID的校验位
            if M == idcard_list[17]:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


def matches(key, value) -> bool:
    username_regex = '^\w{6,10}$'
    password_regex = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,18}$'
    nickname_regex = '^\w{2,8}$'
    mobile_regex = '^\d{11}$'
    email_regex = "^((?!-)[A-Za-z0-9-]" + "{1,63}(?<!-)\\@)" + "+[A-Za-z]{2,6}\\." + "[a-z]{2,3}"

    _dict = {
        'username': username_regex,
        'password': password_regex,
        'nickname': nickname_regex,
        'mobile': mobile_regex,
        'email': email_regex
    }
    if key in _dict.keys():
        return re.match(_dict[key], value) is not None
    elif key == 'document_number':
        return match_document_number(value)
    else:
        return True


def register_params_check(content: dict):
    keys = [
        'username',
        'password',
        'nickname',
        'document_number',
        'mobile',
        'email',
    ]

    if content is None:
        return 'username', False

    for key in keys:
        if not key in content.keys():
            return key, False
        if not matches(key, content[key]):
            return key, False

    return "ok", True
