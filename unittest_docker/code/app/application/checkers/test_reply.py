import unittest
from .reply import reply_post_params_check


class ReplyCheckTest(unittest.TestCase):
    def test_none(self):
        self.assertEqual(reply_post_params_check(None), ('replyId', False))

    def test_no_content(self):
        params = {
            'replyId': "11233"
        }
        self.assertEqual(reply_post_params_check(params), ('content', False))

    def test_overlong_content(self):
        params = {
            'replyId': "11233",
            'content': "The case is being investigated under Section 15 and Section 22(B) of the Prevention and Control of Infectious Diseases Act 1988 for not complying with a valid directive issued by any authoritative officer.Earlier this month, Deputy Inspector-General of Police Datuk Seri Acryl Sani Abdullah Sani was reported as saying that the police were looking into a case involving Amiruddin over his apology through social media for breaking the home quarantine."
        }
        self.assertEqual(reply_post_params_check(params), ('content', False))

    def test_short_content(self):
        params = {
            'replyId': "1234",
            'content': "Short content"
        }
        self.assertEqual(reply_post_params_check(params), ('content', False))

    def test_invalid_replyId(self):
        params = {
            'content': "Writing random content to conduct test"
        }
        invalid_replyId = [
            '-1234',
            '1234a',
            '123456abc',
        ]
        for p in invalid_replyId:
            params['password'] = p
            self.assertEqual(reply_post_params_check(params), ('replyId', False))


if __name__ == '__main__':
    unittest.main()
