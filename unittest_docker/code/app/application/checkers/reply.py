def check_length(key, value) -> bool:
    if key == 'content':
        if len(value) < 15 or len(value) > 256 or len(value) == 0:
            return False
        else:
            return True
    elif key == 'replyId':
        if not value.isnumeric():
            return False
        elif value.isnumeric():
            if int(value) < 0:
                return False
        else:
            return True


def reply_post_params_check(content):
    keys = [
        'content',
        'replyId'
    ]

    if content is None:
        return 'replyId', False

    for key in keys:
        if not key in content.keys():
            return key, False
        if not check_length(key, content[key]):
            return key, False

    return "ok", True
