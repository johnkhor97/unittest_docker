import unittest
from .post import post_params_check


class PostCheckTest(unittest.TestCase):
    def test_none(self):
        self.assertEqual(post_params_check(None), ('title', False))

    def test_no_title(self):
        params = {
            'content': "Dr Noor Hisham’s call came amid a large number of positive cases recorded daily following the Sabah state election and the possible announcement of an Emergency by Prime Minister Tan Sri Muhyiddin Yassin on Saturday."
        }
        self.assertEqual(post_params_check(params), ('title', False))

    def test_overlong_title(self):
        params = {
            'title': "Covid-19: Health DG pleads for unity, warns some frontliners ready to throw in the towel (updated)",
            'content': "Dr Noor Hisham’s call came amid a large number of positive cases."
        }
        self.assertEqual(post_params_check(params), ('title', False))

    def test_no_content(self):
        params = {
            'title': "Testing normal title"
        }
        self.assertEqual(post_params_check(params), ('content', False))

    def test_overlong_content(self):
        params = {
            'title': "Testing normal title",
            'content': "The case is being investigated under Section 15 and Section 22(B) of the Prevention and Control of Infectious Diseases Act 1988 for not complying with a valid directive issued by any authoritative officer.Earlier this month, Deputy Inspector-General of Police Datuk Seri Acryl Sani Abdullah Sani was reported as saying that the police were looking into a case involving Amiruddin over his apology through social media for breaking the home quarantine.",
        }
        self.assertEqual(post_params_check(params), ('content', False))


    def test_short_content(self):
        params = {
            'title': "Testing short content",
            'content':"Short content"
        }
        self.assertEqual(post_params_check(params), ('content', False))


if __name__ == '__main__':
    unittest.main()
