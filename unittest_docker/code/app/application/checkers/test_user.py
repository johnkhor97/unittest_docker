import re
import unittest
from .user import register_params_check


class UserCheckTest(unittest.TestCase):

    def test_none(self):
        self.assertEqual(register_params_check(None), ('username', False))

    def test_no_username(self):
        params = {
            'password': 'password1',
            'nickname': 'name1',
            'document_number': '110101199610258470',
            'mobile': '12312123434',
            'email': 'xxx@163.com',
        }
        self.assertEqual(register_params_check(params), ('username', False))

    def test_no_password(self):
        params = {
            'username': 'username1',
            'nickname': 'name1',
            'document_number': '110101199610258913',
            'mobile': '15878788989',
            'email': 'xxx@gmail.com'
        }
        self.assertEqual(register_params_check(params), ('password', False))

    def test_no_nickname(self):
        params = {
            'username': 'username1',
            'password': 'Password1',
            'document_number': '110101199610258972',
            'mobile': '18711112222',
            'email': 'xxx@xiaomi.com'
        }
        self.assertEqual(register_params_check(params), ('nickname', False))

    def test_no_document_number(self):
        params = {
            'username': 'username1',
            'password': 'Password1',
            'nickname': 'name1',
            'mobile': '18711112222',
            'email': 'xxx@huawei.com'
        }
        self.assertEqual(register_params_check(params), ('document_number', False))

    def test_no_mobile(self):
        params = {
            'username': 'username1',
            'password': 'Password1',
            'document_number': '110101199610257531',
            'nickname': 'name1',
            'email': 'xxx@bytedance.com'
        }
        self.assertEqual(register_params_check(params), ('mobile', False))

    def test_no_email(self):
        params = {
            'username': 'username1',
            'password': 'Password1',
            'document_number': '110101198610256657',
            'nickname': 'name1',
            'mobile': '18711112222',
        }
        self.assertEqual(register_params_check(params), ('email', False))

    def test_illegal_username(self):
        params = {
            'password': 'ABCabc123',
            'nickname': 'Lymntrix',
            'document_number': '110101198610259719',
            'mobile': '12312123434',
            'email': '1293718693@qq.com'
        }
        invalid_usernames = [
            'Short',  # too short (less than 6 characters)
            'LongLongLong',  # too long (longer than 10 characters)
        ]
        for name in invalid_usernames:
            params['username'] = name
            self.assertEqual(register_params_check(params), ('username', False))

    def test_illegal_password(self):
        params = {
            'username': 'Chinwer',
            'nickname': 'Lymntrix',
            'document_number': '110101198610257297',
            'mobile': '1234567890',
            'email': '2263718863@qq.com'
        }
        invalid_passwords = [
            'Short',
            'LongLongLongLongLong',
            'no-uppercase1',
            'NO-LOWERCASE1',
            'No-digital',
        ]
        for p in invalid_passwords:
            params['password'] = p
            self.assertEqual(register_params_check(params), ('password', False))

    def test_illegal_nickname(self):
        params = {
            'username': 'Chinwer',
            'password': 'ABCabc123',
            'document_number': '110101198610257772',
            'mobile': '98765432100',
            'email': '2293718663@qq.com'
        }
        invalid_nicknames = [
            'S',  # too short
            'LongLongLong',  # too long
        ]
        for name in invalid_nicknames:
            params['nickname'] = name
            self.assertEqual(register_params_check(params), ('nickname', False))

    def test_illegal_mobile(self):
        params = {
            'username': 'test123',
            'password': 'ABCabc123',
            'nickname': 'Lymntrix',
            'document_number': '110101198610256833',
            'email': '2293718663@qq.com'
        }
        invalid_mobiles = [
            's123456789',  # containing non-digital character
            '1234567890',  # not 11-digit number
        ]
        for m in invalid_mobiles:
            params['mobile'] = m
            self.assertEqual(register_params_check(params), ('mobile', False))

    def test_illegal_document_number(self):
        params = {
            'username': 'Chinwer',
            'password': 'ABCabc123',
            'nickname': 'Lymntrix',
            'mobile': '98765432100',
            'email': '2293718663@qq.com'
        }
        invalid_document_number = [
            '660101198710248451',  # invalid area number
            '110101201410248577',  # age smaller than 18
        ]
        for m in invalid_document_number:
            params['document_number'] = m
            self.assertEqual(register_params_check(params), ('document_number', False))

    def test_illegal_email(self):
        params = {
            'username': 'Chinwer',
            'password': 'ABCabc123',
            'nickname': 'Lymntrix',
            'document_number': '11010119861025917X',
            'mobile': '98765432100',
        }
        invalid_email_address = [
            'jkhor@gmail.123',  # end with numbers, not .com
            'Jkhor-@gmail.com',  # end with hyphen
        ]
        for m in invalid_email_address:
            params['email'] = m
            self.assertEqual(register_params_check(params), ('email', False))


if __name__ == '__main__':
    unittest.main()
